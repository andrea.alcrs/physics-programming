using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestructableObject : MonoBehaviour
{
    
    public GameObject solid;
    public GameObject destructable;
    public GameObject explosive;
    public float destroyForceLimit = 6f;

    private Transform pieces;
    private UIController UIController;


    private void Start()
    {
        int children = destructable.transform.childCount;

        for (int i = 0; i < children; ++i)
        {
            Transform child = destructable.transform.GetChild(i);
            if (child.CompareTag("Explosive"))
                explosive = child.gameObject;
            if (child.CompareTag("Pieces")) {
                pieces = child;
            }

        }
        UIController = GameObject.FindGameObjectWithTag("UI").GetComponent<UIController>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile") || collision.relativeVelocity.y >= destroyForceLimit)
        {
            DestroyObject();
        }
    }

    private void DestroyObject()
    {
   
        Destroy(solid);
        destructable.SetActive(true);
        List<Transform> childrenList = new List<Transform>();
        int children = pieces.childCount;
        for (int i = 0; i < children; ++i)
        {
            Transform child = pieces.GetChild(i);
            child.GetComponent<Rigidbody>().isKinematic = false;
            childrenList.Add(child);
        }
        foreach (Transform child in childrenList)
        {
            child.parent = null;
            //Destroy(child.gameObject,4f);
        }

        if (explosive)
        {
            explosive.SetActive(true);
            Destroy(explosive, 0.2f);
        }
        Destroy(gameObject, 0.2f);
    }
    private void OnDestroy()
    {
        UIController.ScorePlus();
    }
}
