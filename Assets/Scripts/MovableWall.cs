using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableWall : MonoBehaviour
{
    public float speed = 5;
    public Transform startPoint,endPoint;
    private Vector3 target;

    // Start is called before the first frame update
    void Start()
    {
        target = startPoint.position;
    }

    // Update is called once per frame
    void Update()
    {   
        if (transform.position.Equals(startPoint.position)) {
            target = endPoint.position;
        }
        else if (transform.position.Equals(endPoint.position)) {
            target = startPoint.position;
        }
        
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, step);
    }
}
