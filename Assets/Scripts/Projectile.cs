using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public void DestroyAfterShoot() {
        Destroy(gameObject, 20f);
    }
}
