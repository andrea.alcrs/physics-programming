using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SlingShot : MonoBehaviour
{
    public LineRenderer[] lineRenderers;
    public Transform[] stripPositions;
    public Transform center;
    public Transform idlePosition;
    public UIController UIController;

    public Vector3 currentPosition;

    bool isMouseDown;

    public GameObject[] projectiles;

    public float projectilePositionOffset;

    Rigidbody projectile;
    Collider projectileCollider;

    public int force;

    private int actualIndex = 0;

    void Start()
    {
        lineRenderers[0].positionCount = 2;
        lineRenderers[1].positionCount = 2;
        lineRenderers[0].SetPosition(0, stripPositions[0].position);
        lineRenderers[1].SetPosition(0, stripPositions[1].position);
        UIController.SetForce(force);
        StartCoroutine(Createprojectile(actualIndex, 0f));
    }

    IEnumerator Createprojectile(int projectileIndex,float timeToSpawn)
    {
        yield return new WaitForSeconds(timeToSpawn);

        projectile = Instantiate(projectiles[projectileIndex]).GetComponent<Rigidbody>();
        projectileCollider = projectile.GetComponent<Collider>();
        projectileCollider.enabled = false;

        projectile.isKinematic = true;

        ResetStrips();
    }

    void Update()
    {
        CheckShoot();
        if (Input.anyKeyDown && (!Input.GetMouseButtonDown(0) || !Input.GetMouseButtonDown(1))) {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                actualIndex = 0;
                Destroy(projectile.gameObject);
                StartCoroutine(Createprojectile(actualIndex, 0.2f));
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                actualIndex = 1;
                Destroy(projectile.gameObject);
                StartCoroutine(Createprojectile(actualIndex, 0.2f));
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                actualIndex = 2;
                Destroy(projectile.gameObject);
                StartCoroutine(Createprojectile(actualIndex, 0.2f));
            }
            else if (Input.GetKeyDown(KeyCode.KeypadPlus) || Input.GetKeyDown(KeyCode.Plus)) {
                force += 1;
                UIController.SetForce(force);
            }
            else if (Input.GetKeyDown(KeyCode.KeypadMinus) || Input.GetKeyDown(KeyCode.Minus))
            {
                force -= 1;
                UIController.SetForce(force);
            }
            else if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

        }
    }

    private void CheckShoot()
    {
        checkMouse();
        if (isMouseDown)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.CompareTag("AimWall"))
                {
                    currentPosition = hit.point;
                    if (projectile && !projectile.GetComponent<Transform>().position.Equals(currentPosition))
                    {
                        SetStrips(currentPosition);
                    }

                    if (projectileCollider)
                    {
                        projectileCollider.enabled = true;
                    }
                }

            }
        }
        else if (!isMouseDown)
        {
            ResetStrips();
        }
    }

    private void checkMouse()
    {
        if (Input.GetMouseButtonDown(0)) {
            isMouseDown= true;
        } 
        else if (Input.GetMouseButtonUp(0)) {
            isMouseDown= false;
            Shoot();
            currentPosition = idlePosition.position;
        }
    }

    void Shoot()
    {
        if (projectile) {
            projectile.isKinematic = false;
            Vector3 projectileForce = (currentPosition - center.position) * force * -1;
            projectile.velocity = projectileForce;
            projectile.GetComponent<Projectile>().DestroyAfterShoot();

            projectile = null;
            projectileCollider = null;
            StartCoroutine(Createprojectile(actualIndex, 0.5f));
        }
    }

    void ResetStrips()
    {
        currentPosition = idlePosition.position;
        SetStrips(currentPosition);
    }

    void SetStrips(Vector3 position)
    {
        lineRenderers[0].SetPosition(1, position);
        lineRenderers[1].SetPosition(1, position);

        if (projectile)
        {
            Vector3 dir = position - center.position;
            projectile.transform.position = position + dir.normalized  * projectilePositionOffset;
            projectile.transform.right = -dir.normalized;
        }
    }
}
