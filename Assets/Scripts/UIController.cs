using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class UIController : MonoBehaviour
{
    public GameObject tipPanel;
    public Transform targets;
    public GameObject scoreObj;
    public GameObject forceObj;

    private TMPro.TextMeshProUGUI scoreText;
    private TMPro.TextMeshProUGUI forceText;
    private int score = 0;
    private int maxScore = 0;
    private int force = 0; 
    void Start()
    {
        maxScore = targets.childCount;
        scoreText = scoreObj.GetComponent<TMPro.TextMeshProUGUI>();
        forceText = forceObj.GetComponent<TMPro.TextMeshProUGUI>();
        scoreText.text= "Score: " + score + "/" + maxScore;
        forceText.text = "Force: " + force;
        PauseStartGame();
    }

    public void PauseStartGame() {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        else {
            Time.timeScale = 0;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && tipPanel.activeInHierarchy)
        {
            tipPanel.SetActive(false);
            PauseStartGame();
        }
        else if(Input.GetKeyDown(KeyCode.P)) {
            PauseStartGame();
        }

        scoreText.text = "Score: "+score + "/" + maxScore;
        forceText.text = "Force: " + force;
    }

    public void ScorePlus() {
        score += 1;
    }
    public void SetForce(int newForce) {
        force = newForce;
    }
}
